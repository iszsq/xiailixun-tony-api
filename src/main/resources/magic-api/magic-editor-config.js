var MAGIC_EDITOR_CONFIG = {
    title: '小栗旬理发店接口服务',
    defaultTheme: 'dark',
    checkUpdate: false,
    autoSave: true,
    header: {
        skin: false,    // 屏蔽皮肤按钮
        document: true,    // 屏蔽文档按钮
        repo: false,    // 屏蔽gitee和github
        qqGroup: false  // 屏蔽加入QQ群
    }
    // 其它配置参考本页中其它配置项
}
