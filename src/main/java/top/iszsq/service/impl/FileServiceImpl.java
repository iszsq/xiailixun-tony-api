package top.iszsq.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import top.iszsq.base.dto.FileUploadRes;
import top.iszsq.base.exceptions.BizException;
import top.iszsq.service.FileService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

/**
 * 图片上传
 * @author zsq
 * @date 2021/4/30 - 9:28
 */
@Service
public class FileServiceImpl implements FileService {

    /**保存位置**/
    @Value("${upload.savePath}")
    private String savePath;

    /**文件预览地址**/
    @Value("${upload.previewPath}")
    private String previewPath;

    /**http访问地址**/
    @Value("${upload.baseUrl}")
    private String baseUrl;

    /**
     * 七牛图片上传
     * @param file
     * @return
     */
    @Override
    public FileUploadRes qiniuUpload(MultipartFile file) throws Exception{
        // 去除
        return null;
    }

    /**
     * 本地文件上传
     * @param file
     * @param fileName
     * @param fileTypePath 文件类型目录 如：image、file
     * @return
     * @throws Exception
     */
    @Override
    public FileUploadRes nativeUpload(MultipartFile file, String fileName, String fileTypePath) throws Exception{
        if (file==null || file.isEmpty()) {
            throw new BizException("文件为空");
        }

        if (StringUtils.isBlank(fileTypePath)) {
            fileTypePath = "file";
        }
        //二级目录
        String secondPath = fileTypePath + "/" + DateUtil.today() + "/";

        String fullPath = savePath + secondPath;
        //统一路径分隔符
        String uniFullPath = fullPath.replaceAll("\\/", Matcher.quoteReplacement(File.separator));

        File savePathFile = new File(uniFullPath);

        //判断目录是否存在
        if(!savePathFile.exists()){
            //不存在就创建
            savePathFile.mkdirs();
        }

        //获取上传时的文件名
        if( StringUtils.isBlank(fileName) ){
            fileName = file.getOriginalFilename();
        }

        //文件后缀
        String fileSubfix = fileName.substring(fileName.lastIndexOf("."), fileName.length());

//        文件uuid
        String fileUuid = IdUtil.simpleUUID();
        //新文件名
        String newFileName = fileUuid + fileSubfix;

        //注意是路径+文件名
        File targetFile = new File(savePathFile, newFileName);
        boolean createNewFile = targetFile.createNewFile();
        if(!createNewFile){
            throw new BizException("创建文件失败");
        }
        OutputStream os = new FileOutputStream(targetFile);
        IOUtils.copy(file.getInputStream(), os);

        FileUploadRes result = new FileUploadRes();
        result.setPath(baseUrl + secondPath + newFileName);
        result.setUuid(fileUuid);
        return result;
    }

    /**
     * 本地文件上传
     * @param file
     * @param fileName
     * @return
     * @throws Exception
     */
    @Override
    public FileUploadRes nativeUpload(MultipartFile file, String fileName) throws Exception{
        return this.nativeUpload(file, fileName, null);
    }

}
