package top.iszsq.service;

import org.springframework.web.multipart.MultipartFile;
import top.iszsq.base.dto.FileUploadRes;

import java.util.Map;

/**
 * @author zsq
 * @date 2021/4/30 - 9:28
 */
public interface FileService {

    /**
     * 七牛图片上传
     * @param file
     * @return
     */
    FileUploadRes qiniuUpload(MultipartFile file) throws Exception;

    /**
     * 本地文件上传
     * @param file
     * @param fileName
     * @return
     * @throws Exception
     */
    FileUploadRes nativeUpload(MultipartFile file, String fileName) throws Exception;

    /**
     * 本地文件上传
     * @param file
     * @param fileName
     * @return
     * @throws Exception
     */
    FileUploadRes nativeUpload(MultipartFile file, String fileName, String fileTypePath) throws Exception;
}
