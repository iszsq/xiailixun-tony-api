package top.iszsq;

import cn.dev33.satoken.SaManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 接口服务启动类
 * @author zsq
 * @date 2022/2/10 - 20:41
 */
@SpringBootApplication(
        exclude = { SecurityAutoConfiguration.class, }
)
@EnableTransactionManagement(proxyTargetClass = true)
public class XiaoLiXunApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(XiaoLiXunApplication.class, args);
        System.out.println("-----------启动成功");
        System.out.println("Sa-Token配置如下：" + SaManager.getConfig());
        System.out.println("magic-api框架文档地址：https://www.ssssssss.org/magic-api/");
        System.out.println("个人博客网站：http://iszsq.top");
    }

}
