package top.iszsq.system.entity;

/**
 * 系统用户
 * @author zsq
 * @date 2022/2/10 - 23:03
 */
public class SysUser {

    /** id **/
    private Long id;
    /** uid **/
    private String uid;
    /** 头像 **/
    private String avatar;
    /** 账号 **/
    private String username;
    /** 昵称 **/
    private String nickname;
    /** 手机号 **/
    private String phone;
    /** 状态，1：正常，0：无效 **/
    private Integer state;

    public SysUser() {
    }

    @Override
    public String toString() {
        return "SysUser{" +
                "id=" + id +
                ", uid='" + uid + '\'' +
                ", avatar='" + avatar + '\'' +
                ", username='" + username + '\'' +
                ", nickname='" + nickname + '\'' +
                ", phone='" + phone + '\'' +
                ", state=" + state +
                '}';
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

}
