package top.iszsq.magic_api.function;

import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.config.MagicFunction;
import org.ssssssss.script.annotation.Comment;
import org.ssssssss.script.annotation.Function;
import top.iszsq.base.entity.PageDef;
import top.iszsq.system.entity.SysUser;
import top.iszsq.utils.AuthUtils;
import top.iszsq.utils.CamelCaseUtil;
import javax.annotation.Resource;

/**
 * 自定义函数
 * @author zsq
 * @date 2021/12/7 14:44
 **/
@Component
public class MyCustomFunction implements MagicFunction {

    @Resource
    private AuthUtils authUtils;

    /**
     * 当前登录用户
     * 脚本中直接使用 currentUser();
     * @return
     */
    @Function
    @Comment("当前登录用户")
    public SysUser currentUser() {
        return authUtils.currentUser();
    }

    /** 下划线转驼峰 */
    @Function
    @Comment("下划线转驼峰")
    public static String lineToHump(String str) {
        return CamelCaseUtil.lineToHump(str);
    }

    /** 驼峰转下划线 */
    @Function
    @Comment("驼峰转下划线")
    public static String humpToLine(String str) {
        return CamelCaseUtil.humpToLine(str);
    }

    /**
     * 根据分页大小，获取分页定义
     * @param page
     * @param pageSize
     *  */
    @Function
    @Comment("根据分页大小，获取分页定义")
    public static PageDef fmPage(int page, int pageSize) {
        PageDef pageDef = new PageDef(page, pageSize);
        return pageDef;
    }
}
