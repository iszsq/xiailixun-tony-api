package top.iszsq.base.utils;


import top.iszsq.base.entity.Result;
import top.iszsq.base.enums.RenderEnums;

/**
 * 返回结果 工具类
 * @author zsq
 * @date 2021/5/18 16:08
 **/
public class RenderUtils {

    /**
     * 成功
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(){
        RenderEnums successEnum = RenderEnums.success;

        Result<T> render = new Result<>();
        render.setCode(successEnum.getCode());
        render.setMessage(successEnum.getName());
        return render;
    }

    /**
     * 成功
     * @param data 返回数据
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(T data){
        RenderEnums successEnum = RenderEnums.success;

        Result<T> render = new Result<>();
        render.setCode(successEnum.getCode());
        render.setMessage(successEnum.getName());
        render.setData(data);
        return render;
    }

    /**
     * 成功
     * @param message message
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(String message){
        RenderEnums successEnum = RenderEnums.success;

        Result<T> render = new Result<>();
        render.setCode(successEnum.getCode());
        render.setMessage(message);
        return render;
    }

    /**
     * 成功
     * @param data 返回数据
     * @param message 成功提示
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(String message, T data){
        RenderEnums successEnum = RenderEnums.success;

        Result<T> render = new Result<>();
        render.setCode(successEnum.getCode());
        render.setMessage(message);
        render.setData(data);
        return render;
    }

    /**
     * 失败
     * @param message 失败提示
     * @param <T>
     * @return
     */
    public static <T> Result<T> fail(String message){
        RenderEnums failEnum = RenderEnums.fail;

        Result<T> render = new Result<>();
        render.setCode(failEnum.getCode());
        render.setMessage(message);
        return render;
    }

    /**
     * 失败
     * @param data 返回数据
     * @param message 失败提示
     * @param <T>
     * @return
     */
    public static <T> Result<T> fail(String message, T data){
        RenderEnums failEnum = RenderEnums.fail;

        Result<T> render = new Result<>();
        render.setCode(failEnum.getCode());
        render.setMessage(message);
        render.setData(data);
        return render;
    }

    /**
     * 根据枚举返回
     * @param data 返回数据
     * @param <T>
     * @return
     */
    public static <T> Result<T> parse(RenderEnums renderEnums, T data){
        Result<T> render = new Result<>();
        render.setCode(renderEnums.getCode());
        render.setMessage(renderEnums.getName());
        render.setData(data);
        return render;
    }

    /**
     * 根据枚举返回
     * @param <T>
     * @return
     */
    public static <T> Result<T> parse(RenderEnums renderEnums){
        Result<T> render = new Result<>();
        render.setCode(renderEnums.getCode());
        render.setMessage(renderEnums.getName());
        return render;
    }

}
