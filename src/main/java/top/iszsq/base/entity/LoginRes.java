package top.iszsq.base.entity;


import top.iszsq.system.entity.SysUser;

/**
 * 登录后返回数据
 * @author zsq
 * @date 2022/2/10 - 23:37
 */
public class LoginRes {

    /** 用户信息 **/
    private SysUser userInfo;

    private TokenInfo tokenInfo;

    @Override
    public String toString() {
        return "LoginRes{" +
                "userInfo=" + userInfo +
                ", tokenInfo=" + tokenInfo +
                '}';
    }

    public SysUser getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(SysUser userInfo) {
        this.userInfo = userInfo;
    }

    public TokenInfo getTokenInfo() {
        return tokenInfo;
    }

    public void setTokenInfo(TokenInfo tokenInfo) {
        this.tokenInfo = tokenInfo;
    }
}
