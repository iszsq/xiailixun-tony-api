package top.iszsq.base.entity;

/**
 * 分页定义
 * @author zsq
 * @date 2022/1/13 16:48
 **/
public class PageDef {

    private Integer page;
    private Integer pageSize;
    /**数据大小**/
    private Integer limit;
    /**偏移量**/
    private Integer offset;

    public PageDef(int page, int pageSize){
        int limit = pageSize;
        int offset = (page - 1) * pageSize;
        this.limit = limit;
        this.offset = offset;
    }

    @Override
    public String toString() {
        return "PageDef{" +
                "page=" + page +
                ", pageSize=" + pageSize +
                ", limit=" + limit +
                ", offset=" + offset +
                '}';
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
}
