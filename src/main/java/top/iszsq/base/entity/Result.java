package top.iszsq.base.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.iszsq.base.enums.RenderEnums;

import java.io.Serializable;

/**
 * 统一返回值
 */
@ApiModel(value="统一返回",description="统一返回")
public class Result<T> implements Serializable {

    private static final long serialVersionUID = -8475230226188277459L;

    public static final boolean SUCCESS = true;
    public static final boolean ERROR = false;

    //成功标志
    @ApiModelProperty(value = "成功标志", name = "success")
    private boolean success = SUCCESS;

    //返回消息
    @ApiModelProperty(value = "返回消息", name = "message")
    private String message;

    //状态码
    @ApiModelProperty(value = "状态码", name = "code")
    private int code;

    //返回数据
    @ApiModelProperty(value = "返回数据", name = "data")
    private T data;

    public Result() { }

    public Result(T data) {
        this.data = data;
    }

    public Result(boolean success, String message) {
        if (success) {
            this.code = RenderEnums.success.getCode();
        }
        this.success = success;
        this.message = message;
    }



    public Result(boolean success, String message, T data) {
        if (success) {
            this.code = RenderEnums.success.getCode();
        }
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public boolean getSuccess() {
        return success;
    }

    public Result setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public Result setData(T data) {
        this.data = data;
        return this;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
        this.success = (code == RenderEnums.success.getCode());
    }

    //    /**
//     * @Description:     区分   分不分页的返回情况。
//     * @param pageIndex : 页码
//     * @param pageSize : 每页显示条数
//     * @param maps :     查到的数据集合
//     * @param message :  消息
//     **/
//    public static Result page(Integer pageIndex, Integer pageSize,
//                              List<Map<String, Object>> maps, String message)
//    {
//        if(pageIndex!=null&&pageSize!=null){
//            List<Map<String, Object>> maps1 = PageUtil.paging(maps, pageIndex, pageSize);
//            return new Result(message, DataTypeUtil.listConvertMap(maps, maps1, pageIndex, pageSize));
//        }else{
//            return new Result(message, DataTypeUtil.listConvertMap(maps));
//        }
//    }

    @Override
    public String toString() {
        return "Result{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
