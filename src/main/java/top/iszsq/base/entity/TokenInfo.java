package top.iszsq.base.entity;

/**
 * token 数据
 * @author zsq
 * @date 2022/2/10 - 23:38
 */
public class TokenInfo {

    /** 过期时间戳 **/
    private Long expiresIn;

    /** token值 **/
    private String accessToken;

    public Long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
