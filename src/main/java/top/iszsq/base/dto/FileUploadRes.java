package top.iszsq.base.dto;

/**
 * 文件上传返回对象
 * @author zsq
 * @date 2022/2/20 - 12:27
 */
public class FileUploadRes {

    /**文件访问路径**/
    private String path;

    /**文件uuid**/
    private String uuid;

    public FileUploadRes() {
    }

    @Override
    public String toString() {
        return "FileUploadRes{" +
                "path='" + path + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
