package top.iszsq.base.exceptions;


import top.iszsq.base.entity.Result;
import top.iszsq.base.enums.RenderEnums;
import top.iszsq.base.utils.RenderUtils;

/**
 * 自定义的业务异常，如 添加失败、编辑失败等
 * @author zsq
 * @date 2021/5/18 15:27
 **/
public class BizException extends RuntimeException{

    private Result render;

    public BizException(RenderEnums renderEnums, String message){
        super(renderEnums.getName());
        this.render = RenderUtils.parse(renderEnums);
        this.render.setMessage(message);
    }

    public BizException(RenderEnums renderEnums, Object data){
        super(renderEnums.getName());
        this.render = RenderUtils.parse(renderEnums, data);
    }

    public BizException(RenderEnums renderEnums){
        super(renderEnums.getName());
        this.render = RenderUtils.parse(renderEnums);
    }

    public BizException(String message){
        super(message);
        this.render = RenderUtils.fail(message);
    }

    public Result getRender() {
        return render;
    }

    public BizException setData(Object data){
        this.render.setData(data);
        return this;
    }

}
