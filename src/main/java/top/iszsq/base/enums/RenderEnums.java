package top.iszsq.base.enums;

/**
 * 返回结果状态码
 * 定义一些常用的，前端要使用的状态码
 * @author zsq
 * @date 2021/5/18 16:09
 **/
public enum RenderEnums {

    success(200, "成功"),

    //客户端相关错误
    deniedPerms(403, "没有操作权限"),
    codeWrong(410, "验证码错误"),
    repeatError(415, "请勿重复提交"),

    //服务端相关错误
    fail(500, "失败"),
    needLogin(501, "请登录后访问"),
    tokenInvalid(502, "token失效，请重新登录"),
    ;

    /**状态码，成功:200   失败:500 **/
    private int code;

    /**状态名称**/
    private String name;

    RenderEnums(int code, String name){
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "RenderEnums{" +
                "code=" + code +
                ", name='" + name + '\'' +
                '}';
    }

}
