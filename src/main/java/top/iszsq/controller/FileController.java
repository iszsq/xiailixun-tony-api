package top.iszsq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import top.iszsq.base.dto.FileUploadRes;
import top.iszsq.base.entity.Result;
import top.iszsq.base.utils.RenderUtils;
import top.iszsq.service.FileService;

/**
 * @author zsq
 * @date 2020/12/14 - 21:16
 */
@RestController
@RequestMapping("/file")
public class FileController {


    @Autowired
    private FileService fileService;

    /**
     * 上传图片
     * @param file
     */
    @RequestMapping("upload")
    public Result<FileUploadRes> uploadArticleImage(@RequestParam("file") MultipartFile file, String fileName) throws Exception{
        FileUploadRes fileUploadRes = this.fileService.nativeUpload(file, fileName);

        return RenderUtils.success(fileUploadRes);
    }

}
