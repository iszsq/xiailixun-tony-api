package top.iszsq.utils;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;

/**
 * @author zsq
 * @date 2021/5/25 11:00
 **/
public class MyIdUtils {

    //参数1为终端ID
    //参数2为数据中心ID
    public static Snowflake snowflake = IdUtil.getSnowflake(1, 1);

    /**
     * 创建雪花id
     * @return
     */
    public static long createSnowflakeId(){
        long id = snowflake.nextId();
        return id;
    }

    /**
     * 创建雪花id
     * @return
     */
    public static String uuid(){
        return IdUtil.simpleUUID();
    }

}
