package top.iszsq.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

/**
 * @author zsq
 * @date 2021/9/14 18:12
 **/
public class MyDateUtil {

    /**
     * 时间戳秒 转 LocalDateTime
     * @param second
     * @return
     */
    public static LocalDateTime parseLDTBySecond(long second){
        if (second <= 0) {
            return null;
        }
        return parseLDT(second * 1000L);
    }

    /**
     * 时间戳毫秒 转 LocalDateTime
     * @return
     */
    public static LocalDateTime parseLDT(long milli){
        LocalDateTime createdDate = null;
        if (milli <= 0) {
            return null;
        }
        Instant instant = Instant.ofEpochMilli(milli);
        createdDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        return createdDate;
    }

    /**
     * LocalDateTime 转 时间戳秒
     * @return
     */
    public static Long toSecond(LocalDateTime dateTime){
        if (dateTime == null) {
            return null;
        }
        long l = dateTime.toEpochSecond(ZoneOffset.of("+8"));
        return l;
    }

    /**
     * LocalDateTime 转 时间戳毫秒
     * @return
     */
    public static Long toTimeStamp(LocalDateTime dateTime){
        if (dateTime == null) {
            return null;
        }
        long l = dateTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
        return l;
    }

}
