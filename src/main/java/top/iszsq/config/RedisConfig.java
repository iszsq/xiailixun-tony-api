package top.iszsq.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.*;
import java.time.Duration;

/**
 * Redis 配置
 * @author zsq
 * @date 2021/5/19 17:00
 **/
@ConditionalOnProperty(value = "spring.redis.host")
@Configuration
/**开启缓存管理**/
@EnableCaching
public class RedisConfig {

    /**
     * retemplate相关配置
     * @param factory
     * @return
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        // 配置连接工厂
        template.setConnectionFactory(factory);
        Jackson2JsonRedisSerializer<Object> serializer = new Jackson2JsonRedisSerializer<Object>(Object.class);

        template.setValueSerializer(serializer);
        // 使用StringRedisSerializer来序列化和反序列化redis的key值
        template.setKeySerializer(new StringRedisSerializer());

        // 设置hash key 和value序列化模式
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(serializer);


        return template;
    }

    /**
     * 对hash类型的数据操作
     *
     * @param redisTemplate
     * @return
     */
    @Bean
    public HashOperations<String, String, Object> hashOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForHash();
    }

    /**
     * 对redis字符串类型数据操作
     *
     * @param redisTemplate
     * @return
     */
    @Bean
    public ValueOperations<String, Object> valueOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForValue();
    }

    /**
     * 对链表类型的数据操作
     *
     * @param redisTemplate
     * @return
     */
    @Bean
    public ListOperations<String, Object> listOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForList();
    }

    /**
     * 对无序集合类型的数据操作
     *
     * @param redisTemplate
     * @return
     */
    @Bean
    public SetOperations<String, Object> setOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForSet();
    }

    /**
     * 对有序集合类型的数据操作
     *
     * @param redisTemplate
     * @return
     */
    @Bean
    public ZSetOperations<String, Object> zSetOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForZSet();
    }

    /**
     * 配置默认RedisCacheManager
     * @param redisConnectionFactory
     * @return
     */
    @Primary
    @Bean
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
        Duration duration = Duration.ofHours(12);
        RedisCacheManager cacheManager = createRedisCacheManager(redisConnectionFactory, duration);
        return cacheManager;
    }

    /**
     * 有效时间长的 RedisCacheManager
     * @param redisConnectionFactory
     * @return
     */
    @Bean
    public CacheManager longCacheManager(RedisConnectionFactory redisConnectionFactory) {
        Duration duration = Duration.ofHours(24*5);
        RedisCacheManager cacheManager = createRedisCacheManager(redisConnectionFactory, duration);
        return cacheManager;
    }

    /**
     * 有效时间短的 RedisCacheManager
     * @param redisConnectionFactory
     * @return
     */
    @Bean
    public CacheManager shortCacheManager(RedisConnectionFactory redisConnectionFactory) {
        Duration duration = Duration.ofMinutes(5);
        RedisCacheManager cacheManager = createRedisCacheManager(redisConnectionFactory, duration);
        return cacheManager;
    }

    /**
     * 创建redis缓存管理器
     * @param redisConnectionFactory 工厂
     * @param effectiveTime 有效时间
     * @return
     */
    private RedisCacheManager createRedisCacheManager(RedisConnectionFactory redisConnectionFactory,
                                                      Duration effectiveTime){
        // 设置缓存有效时间
        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(effectiveTime);
        //设置key序列化器
        redisCacheConfiguration.serializeKeysWith(
                RedisSerializationContext.SerializationPair.fromSerializer(keySerializer()));
        //设置value序列化器
        redisCacheConfiguration.serializeValuesWith(
                RedisSerializationContext.SerializationPair.fromSerializer((valueSerializer())));

        RedisCacheManager cacheManager = RedisCacheManager
                .builder(RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory))
                .cacheDefaults(redisCacheConfiguration)
                .transactionAware()
                .build();
        return cacheManager;
    }

    /**
     * key序列化器
     * @return
     */
    private RedisSerializer<String> keySerializer() {
        return new StringRedisSerializer();
    }

    /**
     * value序列化器
     * @return
     */
    private RedisSerializer<Object> valueSerializer() {
        return new GenericJackson2JsonRedisSerializer();
    }

}
