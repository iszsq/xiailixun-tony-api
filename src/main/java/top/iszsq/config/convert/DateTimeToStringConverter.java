package top.iszsq.config.convert;

import org.springframework.core.convert.converter.Converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * LocalDateTime 转字符
 * @author zsq
 * @date 2021/8/28 11:34
 **/
public class DateTimeToStringConverter implements Converter<LocalDateTime, String> {

    private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public String convert(LocalDateTime localDateTime) {
        Object value = localDateTime;
        if (value == null) {
            return null;
        }
        String formatText = ((LocalDateTime)value).format(dateFormatter);
        return formatText;
    }

}
