package top.iszsq.config;

import cn.dev33.satoken.jwt.StpLogicJwtForStateless;
import cn.dev33.satoken.stp.StpLogic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * sa-token 鉴权框架配置
 * @author zsq
 * @date 2022/2/10 - 23:07
 */
@Configuration
public class SaTokenConfigure {

    /**
     * Sa-Token 整合 jwt (Style模式)  https://sa-token.dev33.cn/doc/index.html#/plugin/jwt-extend
     * @return
     */
    @Bean
    public StpLogic getStpLogicJwt() {
        return new StpLogicJwtForStateless();
    }

}
