**【全国巡剪】 小栗旬的理发店 后端接口服务**
<table>
    <tr>
        <td><a href="http://iszsq.top/xiaolixun-h5/" target="_blank">在线演示</a></td>
        <td><a href="http://note.youdao.com/noteshare?id=32c3e5d577edf4e9b2c2156b6d396050&sub=0802F12E45894EC395E72519A4AC36E9" target="_blank">安装文档</a></td>
    </tr>
</table>

# 代码库
* uniapp前端：<a href="https://gitee.com/iszsq/xiaolixun-tony-uniapp" target="_blank">xiaolixun-tony-uniapp</a>
* SpringBoot后端：<a href="https://gitee.com/iszsq/xiailixun-tony-api" target="_blank">xiailixun-tony-api</a>

# 演示地址
#### h5在线演示地址：
<a href="http://iszsq.top/xiaolixun-h5/" target="_blank">在线演示</a>

#### 安卓app体验版下载
链接: <a href="https://pan.baidu.com/s/1ciI9fOOj8VEjGhJChvKVFg?pwd=5uik" target="_blank">https://pan.baidu.com/s/1ciI9fOOj8VEjGhJChvKVFg?pwd=5uik</a>  自动提取码: 5uik   


# 页面预览
## 首页
![](./doc/image/preview/index.png)

## 全国巡剪
![](./doc/image/preview/xunjian.jpg)

## 附近门店
![](./doc/image/preview/mendian.jpg)

## 预约
![](./doc/image/preview/yuyue.jpg)

## 发现
![](./doc/image/preview/faxian.jpg)

## 管理后台
![](./doc/image/preview/manage.jpg)

> 等...

# 项目说明
## 小栗旬理发店app 概念、演示版</h2>
<p>-</p>
<p>app只做演示使用，只用来学习</p>
<p>本人后端程序员，只会敲一点点代码，项目仅供参考</p>
<p>项目包含uniapp端、SpringBoot后端接口服务</p>
<p>-</p>

## uniapp端技术栈
<ul>
    <li>vue2、vuex状态管理等全家桶</li>
    <li>uView2.0 组件库</li>
    <li>luch-request http接口请求库，进一步封装，调用接口更简单方便易维护</li>
    <li>引入二维码生成控件</li>
    <li>集成高德地图</li>
    <li>uniapp分包，管理端代码在pages.json中配置了分包</li>
</ul>

<p>-</p>

## SpringBoot接口服务技术栈
<ul>
    <li>JDK1.8、SpringBoot2.x、MySQL5.7</li>
    <li>redis 缓存，城市投票排名</li>
    <li>swagger2 在线接口文档</li>
    <li>Sa-Token 权限认证框架，快速实现jwt登录、单点登录等</li>
    <li>magic-api 在线接口快速开发框架</li>
    <li>hutool 工具类</li>
    <li>编写配置类，个性化定制SpringBoot</li>
</ul>

# 功能模块
<a href="https://docs.qq.com/mind/DREVOWGVORkxpU3NF" target="_blank">在线查看功能模块</a>

![](./doc/image/project-map.png)



# 安装
<a href="http://note.youdao.com/noteshare?id=32c3e5d577edf4e9b2c2156b6d396050&sub=0802F12E45894EC395E72519A4AC36E9" target="_blank">后端代码安装文档</a>

1. 拉取代码
```
git clone https://gitee.com/iszsq/xiailixun-tony-api.git
```
2. idea打开项目

3. 执行sql脚本
```
路径：doc/sql/mysql-script-1.0.sql
```

4. 确认配置环境后运行
```
top.iszsq.XiaoLiXunApplication
```

# 关于我
如果这个项目对你有一点点帮助，点个免费的star吧！

![](./doc/image/zsq-wx-gzh-2.jpg)

如果想查看更多这个项目的信息，或其他项目的信息，欢迎关注我的个人公众号，里面还有我的开源项目。

